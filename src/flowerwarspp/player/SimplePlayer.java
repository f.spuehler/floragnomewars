package flowerwarspp.player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import flowerwarspp.boardmechanics.ColoredFlower;
import flowerwarspp.preset.Flower;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.PlayerType;


/**
 * This is the implementation of a player that chooses its draws in the manner to create
 * gardens as only principle. Therefore it does not chose its moves anymore in pure random manner.
 * However, this strategy is quite simple, therefore the player is still a very basic form of AI.
 */
public class SimplePlayer extends BasicPlayer {

    /**
     * The constructor of the SimplePlayer. Similar to RandomPlayer.
     */
    public SimplePlayer () {
        this.type = PlayerType.ADVANCED_AI_1;
    }

    /**
     * Chooses the next move and deliver it to request() function of the BasicPlayer.
     * @return The next move.
     */
    @Override
    public Move chooseMove() throws Exception {
        List<Move> possibleMoves = new ArrayList<Move>(viewer.getPossibleMoves());

        //Shuffles array, such that later the move will be chosen randomly.
        Collections.shuffle(possibleMoves);

        Move simpleMove = null;
        int maxScore = Integer.MIN_VALUE;
        int thisMoveScore;

        // Evaluate different move Types differently.
        for (Move thisMove : possibleMoves) {
            switch (thisMove.getType()) {
            case Flower:
                thisMoveScore = computeScore(thisMove.getFirstFlower(), thisMove.getSecondFlower());
                break;
            case Ditch:
                thisMoveScore = 0;
                break;
            case End:
                thisMoveScore = -1;
                break;
            default:
                thisMoveScore = -2;
            }

            if (thisMoveScore > maxScore) {
                maxScore = thisMoveScore;
                simpleMove = thisMove;
            }
        }
        if (simpleMove == null) {
            throw new Exception("The move the Simple player wants to make is null!");
        }
        return simpleMove;
    }

    /**
     * This function computes the score of one move.
     * It needs both flowers that are set in the possible move.
     * @param flower1 First flower of the move.
     * @param flower2 Second flower of the move.
     * @return Score integer value.
     * @see flowerwarspp.player.BasicPlayer#chooseMove()
     */
    private int computeScore(Flower flower1, Flower flower2) {
        int score;
        ColoredFlower cFlower1 = myBoard.getAllFlowers().get(flower1.hashCode());
        ColoredFlower cFlower2 = myBoard.getAllFlowers().get(flower2.hashCode());

        int n1 = countNeighbours(cFlower1);
        int n2 = countNeighbours(cFlower2);
        score = (n1 + 1) * (n2 + 1);

        if (cFlower1.isEdgeNeighbor(cFlower2)) {
            score *= 2;
        }
        return score;
    }

    /**
     * This function counts the neighbors of a Flower.
     * @param flower that is colored.
     * @return number of Neighbors
     */
    private int countNeighbours(ColoredFlower flower) {
        int numNeighours = 0;

        for (ColoredFlower coloredFlower : flower.getFlowerEdgeNeighbors()) {
            if (coloredFlower.getFlowerType() == myFlowerColor) {
                numNeighours++;
            }
        }
        return numNeighours;
    }

}
