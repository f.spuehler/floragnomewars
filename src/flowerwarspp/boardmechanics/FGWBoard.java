package flowerwarspp.boardmechanics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

import flowerwarspp.Debug;
import flowerwarspp.preset.Board;
import flowerwarspp.preset.Status;
import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.MoveType;
import flowerwarspp.preset.Flower;
import flowerwarspp.preset.Ditch;

/**
 * The gameboard where the game happens.
 * Possible moves can be generated, done or undone for each player,
 * points can be calculated.
 * @author Felix Spuehler
**/
public class FGWBoard implements Board {
    /** Maximum size of the board. */
    public static final int MAX_VALUE = 31;
    /** Actual size of the board. */
    private final int boardSize;
    /** Current status of the board. */
    private Status currentStatus;
    /** Color of the player that makes the next move. */
    private PlayerColor currentPlayerColor;
    /** Points of the red player. */
    private int pointsRed;
    /** Points of the blue player. */
    private int pointsBlue;

    /** Stack of made moves (of FGWStackObjects). */
    private Stack<FGWStackObject> madeMoves;

    /** Map to all flowers with hash code as key. */
    private HashMap<Integer, ColoredFlower> allFlowers;
    /** Map to all ditches with hash code as key. */
    private HashMap<Integer, ColoredDitch> alldDitches;
    /** Map to all red possible flowers with hash code as key. */
    private HashMap<Integer, ColoredFlower> redPossibleFlowers;
    /** Map to all blue possible flowers with hash code as key. */
    private HashMap<Integer, ColoredFlower> bluePossibleFlowers;

    /** Collection of all red Flowersets. */
    private Collection<FlowerSet> redFlowers;
    /** Collection of all blue Flowersets. */
    private Collection<FlowerSet> blueFlowers;

    /** Map to all red possible ditches with hash code as key. */
    private HashMap<Integer, ColoredDitch> redPossibleDitches;
    /** Map to all blue possible ditches with hash code as key. */
    private HashMap<Integer, ColoredDitch> bluePossibleDitches;
    /** Collection of all red ditches. */
    private Collection<ColoredDitch> redDitches;
    /** Collection of all blue ditches. */
    private Collection<ColoredDitch> blueDitches;

    /** Map of all possible flower moves mapped to their possibleMoveFlower-object. */
    private HashMap<Move, PossibleMoveFlower> possibleMovesFlower;
    /** Map of all possible ditch moves mapped to the possibleMoveDitch-object. */
    private HashMap<Move, PossibleMoveDitch> possibleMovesDitch;

    /**
     * Constructs of the board and
     * sets all maps of flowers, ditches and so on. <br>
     * Generates the possible moves for the first player (red).
     * @param boardSize Size of the board.
     */
    public FGWBoard(int boardSize) {
        this.boardSize = boardSize;
        currentStatus = Status.Ok;
        currentPlayerColor = PlayerColor.Red;
        pointsRed = 0;
        pointsBlue = 0;

        madeMoves = new Stack<FGWStackObject>();

        FGWBoardInit init = new FGWBoardInit(boardSize);
        allFlowers = init.getAllFlowers();
        alldDitches = init.getAllDitches();

        redPossibleFlowers = init.getAllFlowers();
        bluePossibleFlowers = init.getAllFlowers();
        redPossibleDitches = new HashMap<Integer, ColoredDitch>();
        bluePossibleDitches = new HashMap<Integer, ColoredDitch>();

        redFlowers = new ArrayList<FlowerSet>();
        blueFlowers = new ArrayList<FlowerSet>();
        redDitches = new ArrayList<ColoredDitch>();
        blueDitches = new ArrayList<ColoredDitch>();

        possibleMovesFlower = new HashMap<Move, PossibleMoveFlower>();
        possibleMovesDitch = new HashMap<Move, PossibleMoveDitch>();

        generatePossibleMoves();
    }

    /**
     * Returns the viewer of the board.
     * @return FGWBoardViewer.
     */
    @Override
    public FGWBoardViewer viewer() {
        return new FGWBoardViewer(this);
    }

    /**
     * Returns the size of the board.
     * @return Size.
     */
    public int getBoardSize() {
        return boardSize;
    }

    /**
     * Returns the current status of the board.
     * @return Current status.
     */
    public synchronized Status getCurrentStatus() {
        return currentStatus;
    }

    /**
     * Returns the current player color that makes the next move.
     * @return Color of the current player.
     */
    public synchronized PlayerColor getCurrentPlayerColor() {
        return currentPlayerColor;
    }

    /**
     * Returns the points for the specified color.
     * @param color Color that is wanted.
     * @return Points of that color.
     */
    public synchronized int getPoints(PlayerColor color) {
        if (color == PlayerColor.Red) {
            return pointsRed;
        } else {
            return pointsBlue;
        }
    }


    /**
     * Returns the HashMap of all ditches.
     * @return HashMap of ColoredDitches.
     */
    public HashMap<Integer, ColoredDitch> getAllDitches() {
        return alldDitches;
    }

    /**
     * Returns the HashMap of all Flowers.
     * @return HashMap of ColoredFlowers.
     */
    public HashMap<Integer, ColoredFlower> getAllFlowers() {
        return allFlowers;
    }


    /**
     * Returns possible ditch moves.
     * @return Possible ditch moves.
     */
    public synchronized Collection<Move> getPossibleMovesDitch() {
        return possibleMovesDitch.keySet();
    }

    /**
     * Returns all possible flower moves.
     * @return Collection of possible flower moves.
     */
    public synchronized Collection<Move> getPossibleMovesFlower() {
        return possibleMovesFlower.keySet();
    }

    /**
     * Returns all possible moves.
     * @return Collection of Move-objects.
     */
    public synchronized Collection<Move> getPossibleMoves() {
        HashSet<Move> possibleMoves = new HashSet<Move>();

        if (possibleMovesFlower.keySet().isEmpty()) {
            possibleMoves.add(new Move(MoveType.End));
        } else {
            possibleMoves.addAll(possibleMovesFlower.keySet());
        }

        possibleMoves.addAll(possibleMovesDitch.keySet());
        possibleMoves.add(new Move(MoveType.Surrender));

        return possibleMoves;
    }

    /**
     * Returns the colored flowers.
     * @param color Color that is wanted.
     * @return Flowers of that color.
     */
    public synchronized Collection<Flower> getFlowers(PlayerColor color) {
        Collection<FlowerSet> coloredFlowers;
        ArrayList<Flower> flowers = new ArrayList<Flower>();

        if (color == PlayerColor.Red) {
            coloredFlowers = redFlowers;
        } else {
            coloredFlowers = blueFlowers;
        }

        for (FlowerSet fs: coloredFlowers) {
            flowers.addAll(fs.getFlowers());
        }
        return flowers;
    }

    /**
     * Returns the red Flowerset collection.
     * @return Collection of FlowerSets.
     */
    public synchronized Collection<FlowerSet> getRedFlowers() {
        return redFlowers;
    }

    /**
     * Returns the blue Flowerset collection.
     * @return Collection of FlowerSets.
     */
    public synchronized Collection<FlowerSet> getBlueFlowers() {
        return blueFlowers;
    }

    /**
     * Returns the colored ditches..
     * @param color Color that is wanted.
     * @return Ditches of that color.
     */
    public synchronized Collection<Ditch> getDitches(PlayerColor color) {
        Collection<ColoredDitch> coloredDitches;

        if (color == PlayerColor.Red) {
            coloredDitches = redDitches;
        } else {
            coloredDitches = blueDitches;
        }
        return new ArrayList<Ditch>(coloredDitches);
    }

    /**
     * Returns the red ditches.
     * @return Collection of ColoredDitches.
     */
    public synchronized Collection<ColoredDitch> getRedDitches() {
        return redDitches;
    }

    /**
     * Returns the blue ditches.
     * @return Collection of ColoredDitches.
     */
    public synchronized Collection<ColoredDitch> getBlueDitches() {
        return blueDitches;
    }

    /**
     * Returns the red possible flowers map.
     * @return HashMap of ColoredFlowers.
     */
    public synchronized HashMap<Integer, ColoredFlower> getRedPossibleFlowers() {
        return redPossibleFlowers;
    }

    /**
     * Returns the blue possible flowers map.
     * @return HashMap of ColoredFlowers.
     */
    public synchronized HashMap<Integer, ColoredFlower> getBluePossibleFlowers() {
        return bluePossibleFlowers;
    }

    /**
     * Returns the red possible ditches map.
     * @return HashMap of ColoredDitches.
     */
    public synchronized HashMap<Integer, ColoredDitch> getRedPossibleDitches() {
        return redPossibleDitches;
    }

    /**
     * Returns the blue possible ditches map.
     * @return HashMap of ColoredDitches.
     */
    public synchronized HashMap<Integer, ColoredDitch> getBluePossibleDitches() {
        return bluePossibleDitches;
    }

    /**
     * Returns the ColoredFlower corresponding to a flower.
     * @param flower A flower.
     * @return Its corresponding ColoredFlower.
     */
    public ColoredFlower getColoredFlower(Flower flower) {
        return allFlowers.get(flower.hashCode());
    }

    /**
     * Returns the ColoredDitch corresponding to a ditch.
     * @param ditch A ditch.
     * @return Its corresponding ColoredDitch.
     */
    public ColoredDitch getColoredDitch(Ditch ditch) {
        return alldDitches.get(ditch.hashCode());
    }

    /**
     * Makes a move on the board. Illegal Moves are recognized. <br>
     * After setting the move, the player color changes and points, possible flowers/ditches, etc. are updated.
     * @param move The move.
     * @throws IllegalStateException If game is already over.
     */
    @Override
    public void make(final Move move) throws IllegalStateException {
        if (currentStatus != Status.Ok && currentStatus != Status.Illegal) {
            throw new IllegalStateException("Game is already over!");
        }
        MoveType type = move.getType();
        if (type == MoveType.Surrender) {
            if (currentPlayerColor == PlayerColor.Red) {
                currentStatus = Status.BlueWin;
                Debug.print("Red gives up. Blue wins!");
            } else {
                currentStatus = Status.RedWin;
                Debug.print("Blue gives up. Red wins!");
            }
            return;
        }
        if (type == MoveType.End) {
            if (possibleMovesFlower.isEmpty()) {
                if (pointsRed == pointsBlue) {
                    currentStatus = Status.Draw;
                } else if (pointsRed > pointsBlue) {
                    currentStatus = Status.RedWin;
                } else {
                    currentStatus = Status.BlueWin;
                }
                return;
            } else {
                currentStatus = Status.Illegal;
                Debug.print("End-Move not allowed!");
                return;
            }
        }
        if (type == MoveType.Flower && !possibleMovesFlower.containsKey(move)) {
            currentStatus = Status.Illegal;
            Debug.print("Invalid flower move: " + move);
            return;
        }
        if (type == MoveType.Ditch && !possibleMovesDitch.containsKey(move)) {
            currentStatus = Status.Illegal;
            Debug.print("Invalid ditch move: " + move);
            return;
        }
        if (type == MoveType.Flower) {
            Debug.print(currentPlayerColor + " flower move: " + move);
            PossibleMoveFlower pmf = possibleMovesFlower.get(move);
            pmf.make(move);
        } else if (type == MoveType.Ditch) {
            Debug.print(currentPlayerColor + " ditch move: " + move);
            PossibleMoveDitch pmd = possibleMovesDitch.get(move);
            pmd.make(move);
        } else {
            throw new UnsupportedOperationException("Unsupported move type " + type);
        }

        finishTurn();
        generatePossibleMoves();

        if (possibleMovesFlower.isEmpty() && possibleMovesDitch.isEmpty()) {
            if (pointsRed == pointsBlue) {
                currentStatus = Status.Draw;
            } else if (pointsRed > pointsBlue) {
                currentStatus = Status.RedWin;
            } else {
                currentStatus = Status.BlueWin;
            }
        }
    }

    /**
     * The strategy of the intelligent player is to calculate some moves in advance.
     * This method makes the given move on the board and saves it on a stack to
     * "remember" it for later, when it has to be undone again.
     * @param move The move to make.
     */
    public void makeTree(final Move move) {
        Debug.print("makeTree: " + move);

        if (!getPossibleMoves().contains(move)) {
            System.out.println("Move not allowed: " + move);
            System.console().readLine();
            return;
        }

        FGWStackObject stackObject = new FGWStackObject(move, possibleMovesFlower, possibleMovesDitch);
        madeMoves.push(stackObject);
        make(move);
        Debug.print("made: " + move);
    }

    /**
     * Method to undo ("unmake") the last move. This is used during the precaluclation
     * of moves of the intelligent player.
     * @param move The move to unmake.
     * @throws EmptyStackException If the board is empty.
     */
    public void unmakeTree(final Move move) throws EmptyStackException {
        Debug.print("unmakeTree: " + move);

        if (madeMoves.isEmpty()) {
            throw new EmptyStackException();
        }

        FGWStackObject stackObject = madeMoves.pop();
        if (!stackObject.getMove().equals(move)) {
            throw new IllegalMoveException("Wrong move from Stack!");
        }

        if (move.getType() == MoveType.End) {
            currentStatus = Status.Ok;
            return;
        }

        stackObject.getPossibleMoveObject().unmake(move);
        finishTurn();
        generatePossibleMoves();
    }

    /**
     * Calculate points for the current player and swap color for the next turn.
     */
    private synchronized void finishTurn() {
        currentStatus = Status.Ok;
        calculatePoints(currentPlayerColor);
        currentPlayerColor = (currentPlayerColor == PlayerColor.Red) ? PlayerColor.Blue : PlayerColor.Red;
    }

    /** Calculates the points for the given player.
     * @param color Color of the player.
     */
    private void calculatePoints(PlayerColor color) {
        int points = 0;
        ArrayList<FlowerSet> coloredFlowerSets;

        if (color == PlayerColor.Red) {
            coloredFlowerSets = new ArrayList<FlowerSet>(redFlowers);

        } else {
            coloredFlowerSets = new ArrayList<FlowerSet>(blueFlowers);
        }

        while (!coloredFlowerSets.isEmpty()) {
            FlowerSet cur = coloredFlowerSets.get(0);
            int numberOfConnectedGardens = cur.calculateNumberOfConnectedGardens(coloredFlowerSets);
            points += pointsOfconnectedGardens(numberOfConnectedGardens);
        }

        if (color == PlayerColor.Red) {
            pointsRed = points;
        } else {
            pointsBlue = points;
        }
    }

    /**
     * Calculates the points of n connected gardens. <br>
     * Iterativ way for the rule p(n) = p(n-1) + n.
     * @param numberOfConnectedGardens n.
     * @return Points of the set of connected gardens.
     */
    private int pointsOfconnectedGardens(int numberOfConnectedGardens) {
        return numberOfConnectedGardens * (numberOfConnectedGardens + 1) / 2;
    }

    /**
     * Generates possible moves (seperated in flower and ditch moves).
     */
    private synchronized void generatePossibleMoves() {
        Collection<ColoredFlower> possibleFlowers;

        if (currentPlayerColor == PlayerColor.Red) {
            possibleFlowers = redPossibleFlowers.values();
        } else {
            possibleFlowers = bluePossibleFlowers.values();
        }

        possibleMovesFlower = new HashMap<Move, PossibleMoveFlower>();
        for (ColoredFlower c : possibleFlowers) {
            PossibleMoveFlower dummy = new PossibleMoveFlower(this, c);
            for (Move m : dummy.generateMoves()) {
                possibleMovesFlower.put(m, dummy);
            }
        }

        possibleMovesDitch = new HashMap<Move, PossibleMoveDitch>();
        PossibleMoveDitch dummy = new PossibleMoveDitch(this);
        for (Move m : dummy.generateMoves()) {
            possibleMovesDitch.put(m, dummy);
        }
    }

    /**
     * Resets the board to origin.
     */
    public void reset() {
        currentStatus = Status.Ok;
        currentPlayerColor = PlayerColor.Red;
        pointsRed = 0;
        pointsBlue = 0;

        madeMoves.clear();

        redPossibleFlowers.clear();
        bluePossibleFlowers.clear();
        redPossibleDitches.clear();
        bluePossibleDitches.clear();

        redFlowers.clear();
        blueFlowers.clear();
        redDitches.clear();
        blueDitches.clear();

        for (ColoredFlower f : allFlowers.values()) {
            f.setFlowerType(FGWType.UNCOLORED);
            f.setParentFlowerSet(null);
            redPossibleFlowers.put(f.hashCode(), f);
            bluePossibleFlowers.put(f.hashCode(), f);
        }

        for (ColoredDitch d : alldDitches.values()) {
            d.setDitchType(FGWType.UNCOLORED);
        }

        generatePossibleMoves();
    }
}
