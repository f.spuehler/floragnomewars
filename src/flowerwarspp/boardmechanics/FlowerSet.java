package flowerwarspp.boardmechanics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import flowerwarspp.preset.Flower;

/**
* A FlowerSet consists in edge connected colored ColoredFlowers.
* Its size can be between 1 and 4 and all of its members have the same color.
* @author Charlotte Ackva, (Felix Spuehler, Martin Heide)
**/
public class FlowerSet {
    /** Contains the flowers in the FlowerSet */
    private Collection<ColoredFlower> flowers;
    /** Contains the edge neighboring flowers */
    private Collection<ColoredFlower> flowerEdgeNeighbors;
    /**  Contains the node neighboring flowers of the FlowerSet. */
    private Collection<ColoredFlower> flowerNodeNeighbors;
    /**  Contains edge neighboring ditches of the FlowerSet. */
    private Collection<ColoredDitch> ditchEdgeNeighbors;
    /**  Contains node neighboring ditches of the FlowerSet. */
    private Collection<ColoredDitch> ditchNodeNeighbors;
    /** Contains references to other Flowersets which are connected via ditches.
    * It is required that it is not unique(i.e. two FlowerSets can be connected several times)! */
    private Collection<FlowerSet> ditchConnectedFlowerSets;

    /**
     * Constructs a FlowerSet of size 1.
     * @param firstFlower The first flower in the FlowerSet of type ColoredFlower.
     **/
    public FlowerSet(ColoredFlower firstFlower) {
        flowers = new ArrayList<ColoredFlower>(1);
        flowerEdgeNeighbors = new HashSet<ColoredFlower>(firstFlower.getFlowerEdgeNeighbors());
        flowerNodeNeighbors = new HashSet<ColoredFlower>(firstFlower.getFlowerNodeNeighbors());
        ditchEdgeNeighbors = new HashSet<ColoredDitch>(firstFlower.getDitchEdgeNeighbors());

        ditchNodeNeighbors = new HashSet<ColoredDitch>(firstFlower.getDitchNodeNeighbors());
        plant(firstFlower);
        ditchConnectedFlowerSets = new ArrayList<FlowerSet>();
    }

    /**
     * Copies the specified FlowerSet.
     * @param flowerSetToCopy The FlowerSet you want to copy.
     */
    public FlowerSet(FlowerSet flowerSetToCopy) {
        flowerEdgeNeighbors = new HashSet<ColoredFlower>(flowerSetToCopy.getFlowerEdgeNeighbors());
        flowerNodeNeighbors = new HashSet<ColoredFlower>(flowerSetToCopy.getFlowerNodeNeighbors());
        ditchEdgeNeighbors = new HashSet<ColoredDitch>(flowerSetToCopy.getDitchEdgeNeighbors());
        ditchNodeNeighbors = new HashSet<ColoredDitch>(flowerSetToCopy.getDitchNodeNeighbors());
        flowers = new ArrayList<ColoredFlower>(flowerSetToCopy.getFlowers());

        ditchConnectedFlowerSets = new ArrayList<FlowerSet>();
    }

    /**
    * Returns the vector containing all the FlowerSets which
    * are connected to the Object over a ditch.
    * @return Collection of ditch connected FlowerSets.
    */
    public Collection<FlowerSet> getDitchConnectedFlowerSet() {
        return ditchConnectedFlowerSets;
    }

    /**
    * Adds each element of the argument to the collection of ditch connected FlowerSets.
    * @param flowerSets A collection of ditch connected FlowerSets to add.
    */
    public void addToDitchConnectedFlowerSet(Collection<FlowerSet> flowerSets) {
        ditchConnectedFlowerSets.addAll(flowerSets);
    }

    /**
     * Returns the flower edge neighbors of the FlowerSet.
     * @return Collection of ColoredFlowers.
     */
    public Collection<ColoredFlower> getFlowerEdgeNeighbors() {
        return flowerEdgeNeighbors;
    }

    /**
     * Returns the flower node neighbors of the FlowerSet.
     * @return Collection of ColoredFlowers.
     */
    public Collection<ColoredFlower> getFlowerNodeNeighbors() {
        return flowerNodeNeighbors;
    }

    /**
     * Returns the ditch edge neighbors of the FlowerSet.
     * @return Collection of ColoredDitches.
     */
    public Collection<ColoredDitch> getDitchEdgeNeighbors() {
        return ditchEdgeNeighbors;
    }

    /**
     * Returns the ditch node neighbors of the FlowerSet.
     * @return Collection of ColoredDitches.
     */
    public Collection<ColoredDitch> getDitchNodeNeighbors() {
        return ditchNodeNeighbors;
    }

    /**
     * Returns the flowers belonging to the FlowerSet.
     * @return Vector of ColoredFlowers.
     */
    public Collection<ColoredFlower> getFlowers() {
        return flowers;
    }

    /**
     * Returns the size of the FlowerSet.
     * @return Number of flowers.
     */
    public int getSize() {
        return flowers.size();
    }

    /**
     * Checks if a flower is contained in this FlowerSet.
     * @param flower ColoredFlower to be checked.
     * @return True if the ColoredFlower is in the FlowerSet.
     */
    public boolean contains(ColoredFlower flower) {
        return flowers.contains(flower);
    }

    /**
     * Calculates the number of connected gardens recursively.
     * @param coloredFlowerSets Several FlowerSets to check for connection.
     * @return The number of connected gardens.
     */
    public int calculateNumberOfConnectedGardens(Collection<FlowerSet> coloredFlowerSets) {
        int numberOfGardens = 0;
        if (flowers.size() == 4) {
            numberOfGardens++;
        }
        coloredFlowerSets.remove(this);
        for (FlowerSet fs : ditchConnectedFlowerSets) {
            if (coloredFlowerSets.contains(fs)) {
                numberOfGardens += fs.calculateNumberOfConnectedGardens(coloredFlowerSets);
            }
        }

        return numberOfGardens;
    }

    /**
     * checks if this flowerset is connected to the given flowerset (maybe via other flowersets)
     * @param flowerset the flowerset we want to know if it is connected
     * @param visited already visited flowersets
     * @return true if they are connected
     */
    public boolean isConnected(FlowerSet flowerset, Collection<FlowerSet> visited) {
        if (visited == null) {
            visited = new ArrayList<FlowerSet>();
        }

        visited.add(this);

        if (ditchConnectedFlowerSets.contains(flowerset)) {
            return true;
        }


        for (FlowerSet fs : getDitchConnectedFlowerSet()) {
            if (visited.contains(fs)) {
                continue;
            }
            return fs.isConnected(flowerset, visited);

        }
        return false;
    }

    /**
     * Adds a further Flower to the set of Flowers (without setting the parent)
     * Notice: the reference of the new flower to its parentFlowerSet is not set in this method!
     * But in particular, this method updates the neighborhood of the entire FlowerSet.
     * @param furtherFlower ColoredFlower to be added.
     **/
    public void plant(ColoredFlower furtherFlower) {
        if (flowers.size() == 4) {
            throw new IllegalArgumentException("Number of flowers in the FlowerSet is already 4!");
        }
        if (flowers.contains(furtherFlower)) {
            throw new IllegalArgumentException("The Flower is already contained in FlowerSet!");
        }
        flowers.add(furtherFlower);

        // Update flowerEdgeNeighbors of the entire FlowerSet.
        flowerEdgeNeighbors.addAll(furtherFlower.getFlowerEdgeNeighbors());
        flowerEdgeNeighbors.removeAll(flowers);
        // Update flowerNodeNeighbors of the entire FlowerSet.
        flowerNodeNeighbors.addAll(furtherFlower.getFlowerNodeNeighbors());
        flowerNodeNeighbors.removeAll(flowerEdgeNeighbors);
        flowerNodeNeighbors.removeAll(flowers);



        // Update ditchEdgeNeighbors of the entire FlowerSet.
        HashSet<ColoredDitch> copyDitchEdgeNeighbors = new HashSet<ColoredDitch>(ditchEdgeNeighbors);
        // Take the union of both ...
        ditchEdgeNeighbors.addAll(furtherFlower.getDitchEdgeNeighbors());
        // ... without the intersection.
        for (ColoredDitch ditch : copyDitchEdgeNeighbors) {
            if (furtherFlower.getDitchEdgeNeighbors().contains(ditch)) {
                ditchEdgeNeighbors.remove(ditch);
            }
        }
        // Update ditchNodeNeighbors of the entire FlowerSet.
        // Take the union of both without the union of all edgeDitchNeighbors of every flower in the FlowerSet.

        HashSet<ColoredDitch> allDitchEdgeNeighbors = new HashSet<ColoredDitch>();
        for (ColoredFlower flower : flowers) {
            allDitchEdgeNeighbors.addAll(flower.getDitchEdgeNeighbors());
        }

        ditchNodeNeighbors.addAll(furtherFlower.getDitchNodeNeighbors());
        ditchNodeNeighbors.removeAll(allDitchEdgeNeighbors);

    }

    /**
     * Adds another FlowerSet to the FlowerSet, i.e. merges two FlowerSets (without setting the parent). <br>
     * Notice: the references of the flowers of the new FlowerSet to its changed parentFlowerSet are not set in this method! <br>
     * But in particular, this method updates the neighborhood of the entire FlowerSet.
     * @param flowerSetToAdd The FlowerSet to be added.
     **/
    public void plant(FlowerSet flowerSetToAdd) {
        Collection<ColoredFlower> cfToAdd = flowerSetToAdd.getFlowers();
        if (flowers.size() + cfToAdd.size() > 4) {
            throw new IllegalArgumentException("Merging of both FlowerSets not possible because number of elements too high (>4)!");
        }
        for (ColoredFlower cf : cfToAdd) {
            if (flowers.contains(cf)) {
                throw new IllegalArgumentException("One of the Flowers of the argument FlowerSet is already contained in the former FlowerSet!");
            }
        }
        flowers.addAll(cfToAdd);
        // Update flowerEdgeNeighbors of the entire FlowerSet.
        flowerEdgeNeighbors.addAll(flowerSetToAdd.getFlowerEdgeNeighbors());
        flowerEdgeNeighbors.removeAll(flowers);
        // Update flowerNodeNeighbors of the entire FlowerSet.
        flowerNodeNeighbors.addAll(flowerSetToAdd.getFlowerNodeNeighbors());
        flowerNodeNeighbors.removeAll(flowerEdgeNeighbors);
        flowerNodeNeighbors.removeAll(flowers);

        // Update ditchEdgeNeighbors of the entire FlowerSet.
        HashSet<ColoredDitch> copyDitchEdgeNeighbors = new HashSet<ColoredDitch>(ditchEdgeNeighbors);
        // Take the union of both ...
        ditchEdgeNeighbors.addAll(flowerSetToAdd.getDitchEdgeNeighbors());
        // ... without the intersection.
        for (ColoredDitch ditch : copyDitchEdgeNeighbors) {
            if (flowerSetToAdd.getDitchEdgeNeighbors().contains(ditch)) {
                ditchEdgeNeighbors.remove(ditch);
            }
        }

        // Update ditchNodeNeighbors of the entire FlowerSet.
        // Take the union of both without the union of all edgeDitchNeighbors of every flower in the FlowerSet.
        HashSet<ColoredDitch> allDitchEdgeNeighbors = new HashSet<ColoredDitch>();
        for (ColoredFlower flower : flowers) {
            allDitchEdgeNeighbors.addAll(flower.getDitchEdgeNeighbors());
        }
        ditchNodeNeighbors.addAll(flowerSetToAdd.getDitchNodeNeighbors());
        ditchNodeNeighbors.removeAll(allDitchEdgeNeighbors);
    }
}
