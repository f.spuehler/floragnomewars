package flowerwarspp.boardmechanics;

/**
 * A {@code IllegalMoveException} is thrown if a move is not allowed.
 *
 * @author Felix Spuehler
 */
public class IllegalMoveException extends IllegalArgumentException {
    /** Default constructor. */
    public IllegalMoveException() {
        super();
    }

    /** Constructor with a message.
     * @param msg The message.
     */
    public IllegalMoveException(String msg) {
        super(msg);
    }
}
