package flowerwarspp.boardmechanics;

/**
* Defines possible Status Types for ColoredFlower and ColoredDitches.
*
* implements Serializable missing?!
* @author Charlotte Ackva, Felix Spuehler, Martin Heide
**/

public enum FGWType {
    RED,
    BLUE,
    FALLOW,
    UNCOLORED
}
