package flowerwarspp.gui;

import flowerwarspp.preset.Flower;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.Requestable;

/**
 * Provides move inputs to interactive players.
 *
 * This class acts as a broker between interactive players and the actual input
 * source (e. g. mouse clicks).
 *
 * @author Hans-Georg Sommer
 */
public class MoveRequesterGui implements Requestable {
    /** The first flower of a potential next flower move. */
    private Flower flower;
    /** The next move which would be returned by this input source. */
    private Move move;
    /** Only accept input, if set to true. */
    private boolean enabled;

    /**
     * Check if there is already a flower set for the next move.
     *
     * @return {@code true}, if the first flower was already set.
     */
    public synchronized boolean hasFlower() {
        return flower != null;
    }

    /**
     * Set the first flower of a flower move.
     *
     * @param flower
     *        The first flower.
     */
    public synchronized void setFlower(Flower flower) {
        if (enabled) {
            this.flower = flower;
        }
    }

    /**
     * Get the saved flower.
     *
     * @return The saved flower.
     */
    public synchronized Flower getFlower() {
        return flower;
    }

    /**
     * Set a new move.
     *
     * @param move
     *        The {@link Move} to set.
     */
    public synchronized void setMove(Move move) {
        if (enabled) {
            this.move = move;
        }
    }

    /*
     * @see flowerwarspp.preset.Requestable#request()
     */
    @Override
    public Move request() throws Exception {
        move = null;
        enabled = true;
        while (true) { // just in case something notifies this thread before a move was set
            synchronized (this) {
                wait();
                if (move != null) {
                    flower = null;
                    enabled = false;
                    return move;
                }
            }
        }
    }
}
