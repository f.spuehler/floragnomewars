package flowerwarspp.gui;

import flowerwarspp.GameLoop;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * Game specific menu bar.
 *
 * @author Hans-Georg Sommer
 */
public class GameMenu extends JMenuBar {
    /** URL pointing to a description of the game rules. */
    private static final String RULES_URL = "https://gitlab.gwdg.de/app/flowerwarspp/blob/master/specification/rules.md";
    /** URL pointing to the source code repository. */
    private static final String SOURCE_URL = "https://gitlab.gwdg.de/hans-georg.sommer/FloraGnomeWars";
    /** Parent element. */
    private final JFrame parent;
    /** Contains menu items which are not always enabled. */
    private List<JMenuItem> toggleItems;
    /** GameLoop which can be restarted by this menu. */
    private GameLoop loop;

    /**
     * Create a new menu for the given JFrame.
     *
     * @param frame
     *        The parent JFrame object.
     */
    public GameMenu(JFrame frame) {
        parent = frame;
        toggleItems = new ArrayList<>();
        JMenu game = new JMenu("Game");
        game.setMnemonic(KeyEvent.VK_G);
        add(game);

        JMenuItem newGame = new JMenuItem("New game");
        newGame.setMnemonic(KeyEvent.VK_N);
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if (loop.isGameOver()
                        || JOptionPane.showConfirmDialog(null, "Do you really want to restart the game?",
                                    "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    loop.interrupt();
                }
            }
        });
        toggleItems.add(newGame);
        game.add(newGame);

        JMenuItem quit = new JMenuItem("Quit");
        quit.setMnemonic(KeyEvent.VK_Q);
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
        game.add(quit);

        JMenu help = new JMenu("Help");
        help.setMnemonic(KeyEvent.VK_H);
        add(help);

        JMenuItem rules = new JMenuItem("Rules");
        rules.setMnemonic(KeyEvent.VK_R);
        rules.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(new URL(RULES_URL).toURI());
                    } catch (Exception e) {
                        System.out.println("Error: " + e);
                    }
                }
            }
        });
        help.add(rules);

        JMenuItem source = new JMenuItem("Source code");
        source.setMnemonic(KeyEvent.VK_S);
        source.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(new URL(SOURCE_URL).toURI());
                    } catch (Exception e) {
                        System.out.println("Error: " + e);
                    }
                }
            }
        });
        help.add(source);

        JMenuItem about = new JMenuItem("About");
        about.setMnemonic(KeyEvent.VK_A);
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                JOptionPane.showMessageDialog(parent, new AboutPanel(),
                                              "About", JOptionPane.PLAIN_MESSAGE);
            }
        });
        help.add(about);

        for (JMenuItem item : toggleItems) {
            item.setEnabled(false);
        }
    }

    /**
     * Activate all menu elements which shouldn't be enabled at any time.
     *
     * @param loop Main game loop which can by restarted by thei menu.
     */
    public void activate(GameLoop loop) {
        this.loop = loop;
        for (JMenuItem item : toggleItems) {
            item.setEnabled(true);
        }
    }

    /**
     * Deactivate all menu elements which shouldn't be enabled at any time.
     */
    public void deactivate() {
        loop = null;
        for (JMenuItem item : toggleItems) {
            item.setEnabled(false);
        }
    }
}
