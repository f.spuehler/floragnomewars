package flowerwarspp.gui;

import flowerwarspp.Debug;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.swing.JPanel;
import javax.swing.JTextPane;

/**
 * Panel that is shown in the 'about' dialogue.
 *
 * @author Hans-Georg Sommer
 */
public class AboutPanel extends JPanel {
    /**
     * Create a new panel with predefined content.
     */
    public AboutPanel() {
        super(new BorderLayout());
        JTextPane pane = new JTextPane();
        pane.setEditable(false);
        try {
            pane.setPage(getClass().getResource("/flowerwarspp/text/about.html"));
        } catch (IOException e) {
            Debug.print("AboutPanel: could not load content: " + e.getMessage());
        }
        pane.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                try {
                    InputStream buffer = new BufferedInputStream(
                            getClass().getResourceAsStream("/flowerwarspp/sound/fmaa.wav"));
                    AudioInputStream sound = AudioSystem.getAudioInputStream(buffer);
                    AudioFormat format = sound.getFormat();
                    DataLine.Info info = new DataLine.Info(Clip.class, format);
                    Clip clip = (Clip) AudioSystem.getLine(info);
                    clip.open(sound);
                    clip.start();
                } catch (Exception e) {
                    Debug.print("Playback failed: " + e);
                }
            }
        });
        add(pane);
    }
}
